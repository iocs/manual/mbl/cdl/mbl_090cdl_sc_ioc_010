######################################Cryo-WF############################################
################################# Version: 1.0 ##########################################
# Author:  Adalberto Fontoura
# Date:    26-04-2022
# Version: v1.0
# Def file for Water Flush functions
#########################################################################################

############################
#  STATUS BLOCK
############################
define_status_block()

add_digital("CondWtrCTempOK",                             PV_DESC="OK Conditioning Water Temp")
add_digital("CondWtrCFlowOK",                             PV_DESC="OK Conditioning Water Flow")

add_analog("CCondLevelMIN_st",   "REAL",  PV_PREC="2",     PV_DESC="Min Cold Conditioning Level")
add_analog("CCondPressureMAX_st","REAL",  PV_PREC="2",     PV_DESC="Max Cold Conditioning Pressure")
add_analog("CCondCavTempMAX_st", "REAL",  PV_PREC="2",     PV_DESC="Max Cold Conditioning Cav Temp")
add_analog("CondFPCTempMAX_st",  "REAL",  PV_PREC="2",     PV_DESC="Max Conditioning FPC Temp")
add_analog("CondWtrCTempMAX_st", "REAL",  PV_PREC="2",     PV_DESC="Max Conditioning Water Temp")
add_analog("CondWtrCFlowMAX_st", "REAL",  PV_PREC="2",     PV_DESC="Max Conditioning Water Flow")
add_analog("CondWtrCFlowMIN_st", "REAL",  PV_PREC="2",     PV_DESC="Min Conditioning Water Flow")
add_analog("WtrCFS_F_st",        "REAL",  PV_PREC="2",     PV_DESC="Water FS filter time")

#Water Flushing Digitals
add_digital("Flushing_ON",  	 ARCHIVE=True,             PV_DESC="WF Flush ON",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("Flushing_OFF",  	 ARCHIVE=True,             PV_DESC="WF Flush OFF",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Auto",    ARCHIVE=True,             PV_DESC="WF Flush Mode Auto",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Manual",  ARCHIVE=True,             PV_DESC="WF Flush  Mode Manual",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Forced",  ARCHIVE=True,             PV_DESC="WF Flush  Mode Forced",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_01",   ARCHIVE=True,             PV_DESC="WF Conditions reached",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_02",   ARCHIVE=True,             PV_DESC="WF Conditions reached",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_03",   ARCHIVE=True,             PV_DESC="WF Conditions reached",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_04",   ARCHIVE=True,             PV_DESC="WF Conditions reached",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_01",      ARCHIVE=True,             PV_DESC="WF Circuit 1 Blowed",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_02",      ARCHIVE=True,             PV_DESC="WF Circuit 2 Blowed",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_03",      ARCHIVE=True,             PV_DESC="WF Circuit 3 Blowed",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_04",      ARCHIVE=True,             PV_DESC="WF Circuit 4 Blowed",     PV_ONAM="True",           PV_ZNAM="False")

#Water Flushing Timers
add_time("WF_Wait_Timer_ET_01",                            PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_02",                            PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_03",                            PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_04",                            PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")

#Alarms
add_major_alarm("WF_TT_Error",          "WF Temperature Error",         PV_ZNAM="NominalState")
add_major_alarm("WF_PT_Error",          "WF Pressure Error",            PV_ZNAM="NominalState")
add_major_alarm("WF_YCV_Error",         "WF Valve pos Error",           PV_ZNAM="NominalState")
add_major_alarm("WF_FT_Error",          "WF Flow Error",                PV_ZNAM="NominalState")
add_major_alarm("WF_General_Error",     "WF General Error",             PV_ZNAM="NominalState")
add_major_alarm("WF_01_Error",          "Water Error 01",               PV_ZNAM="NominalState")
add_major_alarm("WF_02_Error",          "Water Error 02",               PV_ZNAM="NominalState")
add_major_alarm("WF_03_Error",          "Water Error 03",               PV_ZNAM="NominalState")
add_major_alarm("WF_04_Error",          "Water Error 04",               PV_ZNAM="NominalState")
add_major_alarm("WF_AirLeackage",       "WF Air Leackage",              PV_ZNAM="NominalState")
add_major_alarm("WF_WaterLeackage",     "WF WaterLeackage",             PV_ZNAM="NominalState")
add_major_alarm("WF_WaterOverPressure", "WF Water Over Pressure",       PV_ZNAM="NominalState")
add_major_alarm("WF_AirOverPressure",   "WF Air Over Pressure",         PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_01",  "WF Freezing Circuit 1",        PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_02",  "WF Freezing Circuit 2",        PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_03",  "WF Freezing Circuit 3",        PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_04",  "WF Freezing Circuit 4",        PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################
define_command_block()

#Water Flushing
add_digital("Cmd_WF_Auto",                ARCHIVE=" 1Hz",		PV_DESC="CMD: Auto Mode")
add_digital("Cmd_WF_Manual",              ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Mode")
add_digital("Cmd_WF_Force",               ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Mode")
add_digital("Cmd_WF_ManuOpen",            ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Open")
add_digital("Cmd_WF_ManuClose",           ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Close")
add_digital("Cmd_WF_ForceOpen",           ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Open")
add_digital("Cmd_WF_ForceClose",          ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Close")
add_digital("Cmd_WF_AckAlarm",            ARCHIVE=" 1Hz",		PV_DESC="CMD: Acknowledge All Alarms")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#Water flush
add_analog("Cond_WF_MIN_Temp","REAL",  PV_PREC="2",                         PV_DESC="Min Temp Water to Flush",  PV_EGU="K")
add_time("Cond_WF_BlowTime",                                                PV_DESC="Time to wait until Water Flush",  PV_EGU="ms")
add_time("Cond_WF_WaitTime",                                                PV_DESC="Time to wait until Water Flush",  PV_EGU="ms")

############################
#  MESSAGE BLOCK
############################

add_string("MESSAGE_01", 39)
add_string("MESSAGE_02", 39)
add_string("MESSAGE_03", 39)
add_string("MESSAGE_04", 39)
add_string("MESSAGE_05", 39)

#Others
add_string("message",    39,     PV_NAME="message",   	 PV_DESC="Text message")
